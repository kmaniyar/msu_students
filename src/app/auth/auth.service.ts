import { Injectable } from '@angular/core';

@Injectable()

export class AuthService {
    authData: Object = {};

    storeData(data: any) : void {
        console.log(data);
        this.authData = data;
    }

    getData() : any {
        if (this.authData != null) {
            return this.authData;
        }
        else 
        {
            return '';
        }
    }
}
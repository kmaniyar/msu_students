import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
    selector: 'app-signup',
    templateUrl: 'signup.component.html',
})

export class SignupComponent {
    myForm : FormGroup
    filterForm: FormGroup
    // Data Types
    
    constructor(private fb: FormBuilder, private router: Router, private _authService: AuthService) {
        this.myForm = this.fb.group(
            {
                firstName : ['', Validators.required],
                lastName: [''],
                email: [''],
                mobile: [''],
                gender: [''],
                skills: [''],
                status: ['']
            }
        )
    }

    fillData() : void {
        console.log('The Data are as following');
        console.log(this.myForm.value);

        this._authService.storeData(this.myForm.value);

        this.router.navigateByUrl('/auth/login');
    }

    getData() : void {
        let data = this._authService.getData();
        console.log('data received from service : ', data);
    }
}
import { Component } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html',
})

export class LoginComponent {
    constructor(private _authService: AuthService) {
        
    }

    getDataInLogin() {
        let data = this._authService.getData();
        console.log('data received in login', data);
    }

    sendData() {
        let myObj = {
            firstName: 'Khilen',
            lastName: 'Maniyar'
        };

        this._authService.storeData(myObj);
    }
}
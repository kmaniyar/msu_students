import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { ForgotComponent } from './forgot/forgot.component';
import { SignupComponent } from './signup/signup.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthService } from './auth.service';

const routes: Routes = [
  {
    path : 'login',
    component: LoginComponent
  },
  {
    path: 'forgot',
    component: ForgotComponent
  },
  {
    path: 'sign-up',
    component: SignupComponent
  },
];

@NgModule({
    declarations: [LoginComponent, ForgotComponent, SignupComponent],
    imports: [
      RouterModule.forChild(routes),
      FormsModule,
      ReactiveFormsModule
    ],
    exports: [RouterModule],
    providers: [AuthService]
})
export class AuthModule {
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()

export class HttpService {
    public url = 'http://127.0.0.1:4000/';
    constructor(private _http: HttpClient) {
    }

    getMethodData(endpoint: string) : Observable<any> {
        console.log('Get Method Called from HTTP Service');
        console.log(this.url + endpoint);
        
        let httpHeaders = new HttpHeaders()
        .set('content-type', 'application/json');

        return this._http.get(this.url + endpoint, {headers: httpHeaders});
        // localhost:4000/posts
    }

    postMethodData(endpoint: string, data: any) : any {
        console.log('Post Method Called from HTTP Service');
        console.log(this.url + endpoint);

        let httpHeaders = new HttpHeaders()
        .set('content-type', 'application/json');

        return this._http.post(this.url + endpoint, data, {headers: httpHeaders});

    }

    deleteMethodData(endpoint: string) : any {
        return this._http.delete(this.url + endpoint);
    }

    putMethodData(endpoint: string, data: any) : any {
        console.log('PUT Method Called from HTTP Service');
        console.log(this.url + endpoint);

        let httpHeaders = new HttpHeaders()
        .set('content-type', 'application/json');

        return this._http.put(this.url + endpoint, data, {headers: httpHeaders});

    }
}
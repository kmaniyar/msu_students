import { Injectable } from '@angular/core';
import { HttpService } from '../services/http.service';
import { Observable } from 'rxjs';

@Injectable()
export class PostService {
    constructor(private _httpService: HttpService) {

    }
    getPosts() : Observable<any> {
        console.log('Get Method Called from Post Service');
        return this._httpService.getMethodData('posts');
    }

    getPostsWithId(postId) : Observable<any> {
        console.log('Get Method Called from Post Service');
        console.log('posts/' + postId);
        return this._httpService.getMethodData('posts/' + postId);
    }

    insertPost(data) : Observable<any> {
        return this._httpService.postMethodData('posts', data);
    }

    updatePost(data, postId) : Observable<any> {
        return this._httpService.putMethodData('posts/' + postId,  data);
    }

    deletePost(postId) : Observable<any> {
        return this._httpService.deleteMethodData('posts/' + postId);
    }
}
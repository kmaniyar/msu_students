import { NgModule } from '@angular/core';
import { PostService } from './posts.service';
import { RouterModule, Routes } from '@angular/router';
import { PostsListComponent } from './posts-list/posts-list.component';
import { PostsAddEditComponent } from './posts-add-edit/posts-add-edit.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const postRouts: Routes = [
    {
        path: '',
        component: PostsListComponent
    },
    {
        path: ':helloId',
        component: PostsAddEditComponent
    }
]

@NgModule({
    declarations: [PostsListComponent, PostsAddEditComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(postRouts), 
        FormsModule,
        ReactiveFormsModule
    ],
    exports: [],
    providers: [PostService]
})

export class PostsModule {

}
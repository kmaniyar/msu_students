import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PostService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-posts-add-edit',
    templateUrl: 'posts-add-edit.component.html'
})

export class PostsAddEditComponent implements OnInit {
    post_type: string;
    post_date: string;
    description: string;
    post_title: string;
    consultancy_id: string;
    postId = null;

    myForm: FormGroup

    constructor(private fb: FormBuilder, private _postService: PostService, private _route: Router, private _activateRoute: ActivatedRoute) {
        this.myForm = this.fb.group({
            post_type: [''],
            post_date: [''],
            description: [''],
            post_title: [''],
            consultancy_id: ['']
        })
    }

    ngOnInit(): void {
        this.postId = this._activateRoute.snapshot.paramMap.get('helloId');
        console.log(this.postId);

        if (this.postId == '-1') {
            // New Mode
        }
        else {
            // Edit Mode
            this.getPostData();
        }
    }

    getPostData() : void {
        this._postService.getPostsWithId(this.postId).subscribe(
            res => {
                console.log(res);
                console.log(res.body);
                this.myForm.patchValue(res.body);
            },
            err => {
                console.log(err);
            }
        )
    }

    savePost() : void {
        console.log(this.myForm.value);

        if (this.postId == -1) {
            this._postService.insertPost(this.myForm.value).subscribe(
                res => {
                    console.log(res);
                    this._route.navigateByUrl('posts');
                },
                err => {
                    console.log(err);
                }
            );
        }
        else {
            this._postService.updatePost(this.myForm.value, this.postId).subscribe(
                res => {
                    console.log(res);
                    this._route.navigateByUrl('posts');
                },
                err => {
                    console.log(err);
                }
            );
        }

    }
}
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { PostService } from '../posts.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-posts-list',
    templateUrl: 'posts-list.component.html'
})

export class PostsListComponent implements OnInit {
    postsList: any = [];
    constructor(public _postService: PostService, private _route: Router) {

    }

    ngOnInit() : void {
        this._postService.getPosts().subscribe(
            res=> {
                
                this.postsList = res.body.data;
                console.log(this.postsList);
            },
            err => {

            }
        )
    }

    editData(postId) : void {
        console.log(postId);
        this._route.navigateByUrl(`/posts/${postId}`);
    }

    goToAddNewPost() : void {
        this._route.navigateByUrl('/posts/-1');
    }

    deletePost(postId) : void {
        this.postsList = [];
        this._postService.deletePost(postId).subscribe(

            res => {
                console.log('The Post Deleted Successfully');
                this._postService.getPosts().subscribe(
                    res=> {
                        
                        this.postsList = res.body.data;
                        console.log(this.postsList);
                    },
                    err => {
        
                    }
                )
            }
        )
    }
    
}
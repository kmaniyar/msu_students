import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PostService } from '../posts/posts.service';

// Metadata
@Component({
    // Template Data
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    providers: [PostService]
})

// Class
export class HomeComponent {
    name = 'Khilen'; // any
    title : number;
    myForm : FormGroup
    filterForm: FormGroup
    // Data Types
    employeeData = [];
    
    constructor(private fb: FormBuilder, private router: Router, private _postService: PostService) {
        // this.myForm = this.fb.group(
        //     {
        //         firstName : ['', Validators.required],
        //         lastName: [''],
        //         email: [''],
        //         mobile: [''],
        //         gender: [''],
        //         skills: [''],
        //         status: ['']
        //     }
        // )
    }

    getPostData() : void {
        console.log('Get Method Called from HOME Component');
        this._postService.getPosts().subscribe(
            res => {
                console.log(res);
                this.employeeData = res;
            }
        )
    }
}